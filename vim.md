VIM
=====

## How to install NERDtree:  
1. install [pathogen](https://github.com/tpope/vim-pathogen)
2. git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree  
3. if you what to open a NERDTree automatically when vim starts up add:  

autocmd vimenter * NERDTree  
to your .vimrc file in (~/.vimrc). from same source as step 2

## How to install CtrlP:
1. Clone the plugin into a separate directory
>   $ cd ~/.vim
>   $ git clone https://github.com/ctrlpvim/ctrlp.vim.git bundle/ctrlp.vim
2. Add to your ~/.vimrc:
>   set runtimepath^=~/.vim/bundle/ctrlp.vim
3. Run at Vim's command line:
>   :helptags ~/.vim/bundle/ctrlp.vim/doc

Once CtrlP is open:

* Press <F5> to purge the cache for the current directory to get new files, remove deleted files and apply new ignore options.
* Press <c-f> and <c-b> to cycle between modes.
* Press <c-d> to switch to filename search instead of full path.
* Press <c-r> to switch to regexp mode.
* Use <c-j>, <c-k> or the arrow keys to navigate the result list.
* Use <c-t> or <c-v>, <c-x> to open the selected entry in a new tab or in a new split.
* Use <c-n>, <c-p> to select the next/previous string in the prompt's history.
* Use <c-y> to create a new file and its parent directories.
* Use <c-z> to mark/unmark multiple files and <c-o> to open them.

