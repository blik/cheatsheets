Git
=======

## get base of 2 commits
git merge-base A B

## get number of commits between two commitishes
git rev-list --count A..B

## check which .gitignore file ignores A
git check-ignore -v A
